# Fleeting Plugin Azure

This is a [fleeting](https://gitlab.com/gitlab-org/fleeting/fleeting) plugin for the Azure Platform.

The Azure plugin can be pointed to a [virtual machine scale set](https://learn.microsoft.com/en-us/azure/virtual-machine-scale-sets/overview), allowing
tasks executed with `fleeting` to be assigned to autoscaled instances.

[![Pipeline Status](https://gitlab.com/gitlab-org/fleeting/fleeting-plugin-azure/badges/main/pipeline.svg)](https://gitlab.com/gitlab-org/fleeting/fleeting-plugin-azure/commits/main)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/gitlab-org/fleeting/fleeting-plugin-azure)](https://goreportcard.com/report/gitlab.com/gitlab-org/fleeting/fleeting-plugin-azure)

## Build the plugin

To generate the binary:

1. Ensure `$GOPATH/bin` is on your PATH.
1. Generate the binary:

   ```shell
   cd cmd/fleeting-plugin-azure/
   go install 
   ```

1. Optional. If you manage Go versions with asdf, run the following command after you generate the binary:

   ```shell
   asdf reshim
   ```

## Plugin configuration

The following parameters are supported:

| Parameter             | Type   | Description |
|-----------------------|--------|-------------|
| `name`                | string | Name of the scale set. |
| `cloud`               | string | Type of cloud `['public', 'government', 'china']`. Optional, default to `public` |
| `subscription_id`     | string | Azure subscription ID. |
| `resource_group_name` | string | Azure resource group name. |

### Default connector config

| Parameter                | Default  |
|--------------------------|----------|
| `os`                     | `linux`  |
| `username`               | Determined by the OSProfile, otherwise the default is `azureuser`. |
| `password`               | Determined by the OSProfile, otherwise the default is blank.
| `protocol`               | `ssh` or `winrm` if Windows OS is detected. |
| `use_static_credentials` | `false`  |

## WinRM

The fleeting connector can use Basic authentication with WinRM-HTTP (TCP/5985) to connect to the Azure instance.

The following WinRM settings must be used:

```powershell
winrm quickconfig -q -force
winrm set winrm/config/service/Auth '@{Basic="true"}'
winrm set winrm/config '@{MaxTimeoutms="7200000"}'
winrm set winrm/config '@{MaxEnvelopeSizekb="8192"}'
winrm set winrm/config/winrs '@{MaxMemoryPerShellMB="0"}'
winrm set winrm/config/winrs '@{MaxProcessesPerShell="0"}'
winrm set winrm/config/service '@{AllowUnencrypted="true"}'
winrm set winrm/config/service '@{MaxConcurrentOperationsPerUser="12000"}'
New-NetFirewallRule -DisplayName "Allow inbound WinRM" -Direction Inbound -LocalPort 5985-5986 -Protocol TCP -Action Allow

# Optionally configure UAC to allow privilege elevation in remote shells
$key = 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System'
$setting = 'LocalAccountTokenFilterPolicy'
Set-ItemProperty -Path $key -Name $setting -Value 1 -Force
```

This adjusts the firewall, and allows Basic authentication with an unencrypted connection (WinRM-HTTP).

## Examples

### GitLab Runner

GitLab Runner has examples about using this plugin for the [Instance executor](https://docs.gitlab.com/runner/executors/instance.html#examples) and [Docker Autoscaler executor](https://docs.gitlab.com/runner/executors/docker_autoscaler.html#examples).
