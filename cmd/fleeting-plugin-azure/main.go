package main

import (
	azure "gitlab.com/gitlab-org/fleeting/fleeting-plugin-azure"
	"gitlab.com/gitlab-org/fleeting/fleeting/plugin"
)

func main() {
	plugin.Main(&azure.InstanceGroup{}, azure.Version)
}
