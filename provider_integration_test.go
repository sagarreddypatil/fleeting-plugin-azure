package azure

import (
	"context"
	"crypto/rand"
	"encoding/hex"
	"io"
	"os"
	"testing"
	"time"

	"github.com/Azure/azure-sdk-for-go/sdk/azcore/to"
	"github.com/Azure/azure-sdk-for-go/sdk/azidentity"
	"github.com/Azure/azure-sdk-for-go/sdk/resourcemanager/compute/armcompute/v4"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/fleeting/fleeting/integration"
	"gitlab.com/gitlab-org/fleeting/fleeting/provider"
)

func TestProvisioning(t *testing.T) {
	integrationTestSubscriptionID := os.Getenv("AZURE_SUBSCRIPTION_ID")
	if integrationTestSubscriptionID == "" {
		t.Skip("no integration test subscription id defined")
	}

	integrationTestResourceGroupName := os.Getenv("AZURE_RESOURCE_GROUP_NAME")
	if integrationTestResourceGroupName == "" {
		t.Skip("no integration resource group name defined")
	}

	// az network vnet show --resource-group ajwalker-fleeting-plugin --name ajwalker-fleeting-plugin-vnet
	integreationTestSubnetID := os.Getenv("AZURE_SUBNET_ID")
	if integreationTestSubnetID == "" {
		t.Skip("no integration subnet id defined")
	}

	cred, err := azidentity.NewDefaultAzureCredential(nil)
	require.NoError(t, err)

	client, err := armcompute.NewVirtualMachineScaleSetsClient(integrationTestSubscriptionID, cred, nil)
	require.NoError(t, err)

	name := uniqueScaleSetName()
	password := uniquePassword()

	ctx := context.Background()

	// good examples o how to create a scale set here: https://learn.microsoft.com/en-us/rest/api/compute/virtual-machine-scale-sets/create-or-update?view=rest-compute-2023-07-01
	createResp, err := client.BeginCreateOrUpdate(ctx, integrationTestResourceGroupName, name, armcompute.VirtualMachineScaleSet{
		// find valid regions: az account list-locations -o table
		Location: to.Ptr("eastus"),

		Properties: &armcompute.VirtualMachineScaleSetProperties{
			Overprovision: to.Ptr(false),

			UpgradePolicy: &armcompute.UpgradePolicy{
				Mode: to.Ptr(armcompute.UpgradeModeManual),
			},

			VirtualMachineProfile: &armcompute.VirtualMachineScaleSetVMProfile{
				StorageProfile: &armcompute.VirtualMachineScaleSetStorageProfile{
					// find ubuntu image with: az vm image list -p Canonical --sku 23_10 --all
					ImageReference: &armcompute.ImageReference{
						Publisher: to.Ptr("Canonical"),
						Offer:     to.Ptr("0001-com-ubuntu-minimal-mantic"),
						SKU:       to.Ptr("minimal-23_10-gen2"),
						Version:   to.Ptr("23.10.202310110"),
					},
				},

				OSProfile: &armcompute.VirtualMachineScaleSetOSProfile{
					ComputerNamePrefix: to.Ptr("fleeting"),
					AdminUsername:      to.Ptr("ubuntu"),
					AdminPassword:      to.Ptr(password),
				},

				NetworkProfile: &armcompute.VirtualMachineScaleSetNetworkProfile{
					NetworkInterfaceConfigurations: []*armcompute.VirtualMachineScaleSetNetworkConfiguration{
						{
							Name: to.Ptr(name),
							Properties: &armcompute.VirtualMachineScaleSetNetworkConfigurationProperties{
								EnableIPForwarding: to.Ptr(true),
								IPConfigurations: []*armcompute.VirtualMachineScaleSetIPConfiguration{
									{
										Name: to.Ptr(name),
										Properties: &armcompute.VirtualMachineScaleSetIPConfigurationProperties{
											Subnet: &armcompute.APIEntityReference{
												ID: to.Ptr(integreationTestSubnetID),
											},
											PublicIPAddressConfiguration: &armcompute.VirtualMachineScaleSetPublicIPAddressConfiguration{
												Name:       to.Ptr(name),
												Properties: &armcompute.VirtualMachineScaleSetPublicIPAddressConfigurationProperties{
													//DeleteOption: to.Ptr(armcompute.DeleteOptionsDelete),
												},
											},
										},
									}},
								Primary: to.Ptr(true),
							},
						}},
				},
			},
		},

		SKU: &armcompute.SKU{
			Capacity: to.Ptr(int64(0)),
			Name:     to.Ptr("Standard_B1s"),
			Tier:     to.Ptr("standard"),
		},
	}, &armcompute.VirtualMachineScaleSetsClientBeginCreateOrUpdateOptions{})
	require.NoError(t, err)

	_, err = createResp.PollUntilDone(ctx, nil)
	require.NoError(t, err)

	defer func() {
		deleteResp, err := client.BeginDelete(ctx, integrationTestResourceGroupName, name, &armcompute.VirtualMachineScaleSetsClientBeginDeleteOptions{ForceDeletion: to.Ptr(true)})
		require.NoError(t, err)

		_, err = deleteResp.PollUntilDone(ctx, nil)
		require.NoError(t, err)
	}()

	integration.TestProvisioning(t,
		integration.BuildPluginBinary(t, "cmd/fleeting-plugin-azure", "fleeting-plugin-azure"),
		integration.Config{
			PluginConfig: InstanceGroup{
				Name:              name,
				SubscriptionID:    integrationTestSubscriptionID,
				ResourceGroupName: integrationTestResourceGroupName,
			},
			ConnectorConfig: provider.ConnectorConfig{
				Username: "ubuntu",
				Password: password,
				Timeout:  10 * time.Minute,
			},
			MaxInstances:    3,
			UseExternalAddr: true,
		},
	)
}

func uniqueScaleSetName() string {
	var buf [8]byte
	io.ReadFull(rand.Reader, buf[:])

	return "azure-fleeting-integration-" + hex.EncodeToString(buf[:])
}

func uniquePassword() string {
	var buf [16]byte
	io.ReadFull(rand.Reader, buf[:])

	return "P!" + hex.EncodeToString(buf[:])
}
