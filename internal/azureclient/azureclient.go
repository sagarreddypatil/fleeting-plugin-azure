package azureclient

import (
	"context"
	"fmt"

	"github.com/Azure/azure-sdk-for-go/sdk/azcore"
	"github.com/Azure/azure-sdk-for-go/sdk/azcore/arm"
	"github.com/Azure/azure-sdk-for-go/sdk/azcore/cloud"
	"github.com/Azure/azure-sdk-for-go/sdk/azcore/runtime"
	"github.com/Azure/azure-sdk-for-go/sdk/azidentity"
	"github.com/Azure/azure-sdk-for-go/sdk/resourcemanager/compute/armcompute/v4"
	"github.com/Azure/azure-sdk-for-go/sdk/resourcemanager/network/armnetwork/v2"
)

type Client interface {
	GetScaleSetVM(ctx context.Context, resourceGroupName string, vmScaleSetName string, instanceID string, options *armcompute.VirtualMachineScaleSetVMsClientGetOptions) (armcompute.VirtualMachineScaleSetVMsClientGetResponse, error)
	GetScaleSet(ctx context.Context, resourceGroupName string, vmScaleSetName string, options *armcompute.VirtualMachineScaleSetsClientGetOptions) (armcompute.VirtualMachineScaleSetsClientGetResponse, error)
	BeginDeleteInstances(ctx context.Context, resourceGroupName string, vmScaleSetName string, vmInstanceIDs armcompute.VirtualMachineScaleSetVMInstanceRequiredIDs, options *armcompute.VirtualMachineScaleSetsClientBeginDeleteInstancesOptions) (*runtime.Poller[armcompute.VirtualMachineScaleSetsClientDeleteInstancesResponse], error)
	BeginUpdate(ctx context.Context, resourceGroupName string, vmScaleSetName string, parameters armcompute.VirtualMachineScaleSetUpdate, options *armcompute.VirtualMachineScaleSetsClientBeginUpdateOptions) (*runtime.Poller[armcompute.VirtualMachineScaleSetsClientUpdateResponse], error)
	NewListPager(resourceGroupName string, virtualMachineScaleSetName string, options *armcompute.VirtualMachineScaleSetVMsClientListOptions) *runtime.Pager[armcompute.VirtualMachineScaleSetVMsClientListResponse]
	GetVirtualMachineScaleSetNetworkInterface(ctx context.Context, resourceGroupName string, virtualMachineScaleSetName string, virtualmachineIndex string, networkInterfaceName string, options *armnetwork.InterfacesClientGetVirtualMachineScaleSetNetworkInterfaceOptions) (armnetwork.InterfacesClientGetVirtualMachineScaleSetNetworkInterfaceResponse, error)
}

var _ Client = (*client)(nil)

type client struct {
	client    *armcompute.VirtualMachineScaleSetsClient
	vmClient  *armcompute.VirtualMachineScaleSetVMsClient
	netClient *armnetwork.InterfacesClient
}

func New(subscriptionID string, azCloud string) (Client, error) {
	var cloudType cloud.Configuration

	switch azCloud {
	case "public":
		cloudType = cloud.AzurePublic
	case "government":
		cloudType = cloud.AzureGovernment
	case "china":
		cloudType = cloud.AzureChina
	default:
		cloudType = cloud.AzurePublic
	}

    opts := azcore.ClientOptions{Cloud: cloudType}
    cred, err := azidentity.NewDefaultAzureCredential(&azidentity.DefaultAzureCredentialOptions{ClientOptions: opts})

	if err != nil {
		return nil, err
	}

	client := &client{}

    client.client, err = armcompute.NewVirtualMachineScaleSetsClient(subscriptionID, cred, &arm.ClientOptions{ClientOptions: opts})
	if err != nil {
		return nil, fmt.Errorf("creating virtual machine scale sets client: %w", err)
	}

    client.vmClient, err = armcompute.NewVirtualMachineScaleSetVMsClient(subscriptionID, cred, &arm.ClientOptions{ClientOptions: opts})
	if err != nil {
		return nil, fmt.Errorf("creating virtual machine scale sets vms client: %w", err)
	}

    client.netClient, err = armnetwork.NewInterfacesClient(subscriptionID, cred, &arm.ClientOptions{ClientOptions: opts})
	if err != nil {
		return nil, fmt.Errorf("creating network client: %w", err)
	}

	return client, nil
}

func (c *client) GetScaleSetVM(ctx context.Context, resourceGroupName string, vmScaleSetName string, instanceID string, options *armcompute.VirtualMachineScaleSetVMsClientGetOptions) (armcompute.VirtualMachineScaleSetVMsClientGetResponse, error) {
	return c.vmClient.Get(ctx, resourceGroupName, vmScaleSetName, instanceID, options)
}

func (c *client) GetScaleSet(ctx context.Context, resourceGroupName string, vmScaleSetName string, options *armcompute.VirtualMachineScaleSetsClientGetOptions) (armcompute.VirtualMachineScaleSetsClientGetResponse, error) {
	return c.client.Get(ctx, resourceGroupName, vmScaleSetName, options)
}

func (c *client) BeginDeleteInstances(ctx context.Context, resourceGroupName string, vmScaleSetName string, vmInstanceIDs armcompute.VirtualMachineScaleSetVMInstanceRequiredIDs, options *armcompute.VirtualMachineScaleSetsClientBeginDeleteInstancesOptions) (*runtime.Poller[armcompute.VirtualMachineScaleSetsClientDeleteInstancesResponse], error) {
	return c.client.BeginDeleteInstances(ctx, resourceGroupName, vmScaleSetName, vmInstanceIDs, options)
}

func (c *client) BeginUpdate(ctx context.Context, resourceGroupName string, vmScaleSetName string, parameters armcompute.VirtualMachineScaleSetUpdate, options *armcompute.VirtualMachineScaleSetsClientBeginUpdateOptions) (*runtime.Poller[armcompute.VirtualMachineScaleSetsClientUpdateResponse], error) {
	return c.client.BeginUpdate(ctx, resourceGroupName, vmScaleSetName, parameters, options)
}

func (c *client) NewListPager(resourceGroupName string, virtualMachineScaleSetName string, options *armcompute.VirtualMachineScaleSetVMsClientListOptions) *runtime.Pager[armcompute.VirtualMachineScaleSetVMsClientListResponse] {
	return c.vmClient.NewListPager(resourceGroupName, virtualMachineScaleSetName, options)
}

func (c *client) GetVirtualMachineScaleSetNetworkInterface(ctx context.Context, resourceGroupName string, virtualMachineScaleSetName string, virtualmachineIndex string, networkInterfaceName string, options *armnetwork.InterfacesClientGetVirtualMachineScaleSetNetworkInterfaceOptions) (armnetwork.InterfacesClientGetVirtualMachineScaleSetNetworkInterfaceResponse, error) {
	return c.netClient.GetVirtualMachineScaleSetNetworkInterface(ctx, resourceGroupName, virtualMachineScaleSetName, virtualmachineIndex, networkInterfaceName, options)
}
