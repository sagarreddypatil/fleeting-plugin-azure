package azure

import (
	"bytes"
	"context"
	"testing"

	"github.com/hashicorp/go-hclog"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/fleeting/fleeting-plugin-azure/internal/azureclient"
	"gitlab.com/gitlab-org/fleeting/fleeting-plugin-azure/internal/azureclient/fake"
	"gitlab.com/gitlab-org/fleeting/fleeting/provider"
)

func setupFakeClient(t *testing.T, setup func(client *fake.Client)) *InstanceGroup {
	t.Helper()

	oldClient := newClient
	t.Cleanup(func() {
		newClient = oldClient
	})

	newClient = func(subscriptionID string, azCloud string) (azureclient.Client, error) {
		client := fake.New()
		client.Name = "test-group"
		if setup != nil {
			setup(client)
		}

		return client, nil
	}

	return &InstanceGroup{
		Name: "test-group",
	}
}

func TestCapacitySync(t *testing.T) {
	setupFakeClient(t, func(client *fake.Client) {
		client.TargetCapacity = 1
		client.Instances = append(client.Instances, fake.Instance{
			InstanceId:        "pre-existing",
			ProvisioningState: "Succeeded",
			InstanceStatus:    []string{"PowerState/running"},
		})
	})

	ctx := context.Background()

	group := &InstanceGroup{
		Name: "test-group",
	}

	var buf bytes.Buffer
	logger := hclog.NewInterceptLogger(&hclog.LoggerOptions{Output: &buf})

	// initialize with 1 instance
	_, err := group.Init(ctx, logger, provider.Settings{})
	require.NoError(t, err)
	require.NoError(t, group.Update(ctx, func(id string, state provider.State) {}))

	// increase to 5
	num, err := group.Increase(ctx, 5)
	require.Equal(t, 5, num)
	require.NoError(t, err)
	require.NoError(t, group.Update(ctx, func(id string, state provider.State) {}))
	require.Equal(t, 6, group.client.(*fake.Client).TargetCapacity)
	require.Equal(t, 6, group.size)

	// set instance group to have 10 instances manually and create new instance to detect out-of-sync
	group.client.(*fake.Client).TargetCapacity = 10
	num, err = group.Increase(ctx, 1)
	require.Equal(t, 1, num)
	require.NoError(t, err)
	require.Contains(t, buf.String(), "[ERROR] out-of-sync capacity: name=test-group resource-group=\"\" subscription-id=\"\" expected=6 actual=10")
	assert.Equal(t, 11, group.client.(*fake.Client).TargetCapacity)
	assert.Equal(t, 11, group.size)
}

func TestIncrease(t *testing.T) {
	group := setupFakeClient(t, nil)

	ctx := context.Background()

	var count int
	_, err := group.Init(ctx, hclog.NewNullLogger(), provider.Settings{})
	require.NoError(t, err)
	require.NoError(t, group.Update(ctx, func(id string, state provider.State) {
		count++
	}))
	require.Equal(t, 0, group.size)
	require.Equal(t, 0, count)

	num, err := group.Increase(ctx, 2)
	require.NoError(t, err)
	require.Equal(t, 2, num)
	count = 0
	require.NoError(t, group.Update(ctx, func(id string, state provider.State) {
		require.Equal(t, provider.StateRunning, state)
		count++
	}))
	require.Equal(t, 2, group.client.(*fake.Client).TargetCapacity)
	require.Equal(t, 2, group.size)
	require.Equal(t, 2, count)
}

func TestDecrease(t *testing.T) {
	group := setupFakeClient(t, func(client *fake.Client) {
		client.TargetCapacity = 2
		client.Instances = append(
			client.Instances,
			fake.Instance{
				InstanceId:        "pre-existing-1",
				ProvisioningState: "Succeeded",
				InstanceStatus:    []string{"PowerState/running"},
			},
			fake.Instance{
				InstanceId:        "pre-existing-2",
				ProvisioningState: "Succeeded",
				InstanceStatus:    []string{"PowerState/running"},
			},
		)
	})

	ctx := context.Background()

	var count int
	_, err := group.Init(ctx, hclog.NewNullLogger(), provider.Settings{})
	require.NoError(t, err)
	require.NoError(t, group.Update(ctx, func(id string, state provider.State) {
		require.Equal(t, provider.StateRunning, state)
		count++
	}))
	require.Equal(t, 2, group.size)
	require.Equal(t, 2, count)

	removed, err := group.Decrease(ctx, []string{"pre-existing-1"})
	require.NoError(t, err)
	require.Contains(t, removed, "pre-existing-1")
	count = 0
	require.NoError(t, group.Update(ctx, func(id string, state provider.State) {
		count++
	}))
	require.Equal(t, 1, group.client.(*fake.Client).TargetCapacity)
	require.Equal(t, 1, group.size)
}
