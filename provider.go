package azure

import (
	"context"
	"fmt"
	"path"
	"strings"

	"github.com/Azure/azure-sdk-for-go/sdk/azcore/to"
	"github.com/Azure/azure-sdk-for-go/sdk/resourcemanager/compute/armcompute/v4"
	"github.com/Azure/azure-sdk-for-go/sdk/resourcemanager/network/armnetwork/v2"
	"github.com/hashicorp/go-hclog"
	"gitlab.com/gitlab-org/fleeting/fleeting-plugin-azure/internal/azureclient"
	"gitlab.com/gitlab-org/fleeting/fleeting/provider"
)

var _ provider.InstanceGroup = (*InstanceGroup)(nil)

var newClient = azureclient.New

type InstanceGroup struct {
	SubscriptionID    string `json:"subscription_id"`
	ResourceGroupName string `json:"resource_group_name"`
	Cloud			  string `json:"cloud"`
	Name              string `json:"name"`

	size int

	client azureclient.Client

	log hclog.Logger

	settings provider.Settings
}

func deref[T any](v *T) T {
	if v == nil {
		var def T
		return def
	}
	return *v
}

// Init implements provider.InstanceGroup
func (g *InstanceGroup) Init(ctx context.Context, logger hclog.Logger, settings provider.Settings) (provider.ProviderInfo, error) {
	var err error
	g.client, err = newClient(g.SubscriptionID, g.Cloud)
	if err != nil {
		return provider.ProviderInfo{}, fmt.Errorf("creating client: %w", err)
	}

	g.log = logger.With("subscription-id", g.SubscriptionID, "resource-group", g.ResourceGroupName, "name", g.Name)
	g.settings = settings

	if err := g.setScaleSetSize(ctx, true); err != nil {
		return provider.ProviderInfo{}, err
	}

	return provider.ProviderInfo{
		ID:        path.Join("azure", g.SubscriptionID, g.ResourceGroupName, g.Name),
		MaxSize:   1000,
		Version:   Version.String(),
		BuildInfo: Version.BuildInfo(),
	}, nil
}

// ConnectInfo implements provider.InstanceGroup
func (g *InstanceGroup) ConnectInfo(ctx context.Context, id string) (provider.ConnectInfo, error) {
	info := provider.ConnectInfo{ConnectorConfig: g.settings.ConnectorConfig}

	instance, err := g.client.GetScaleSetVM(ctx, g.ResourceGroupName, g.Name, id, nil)
	if err != nil {
		return info, fmt.Errorf("fetching instance: %w", err)
	}

	if info.OS == "" {
		info.OS = strings.ToLower(string(deref(instance.Properties.StorageProfile.OSDisk.OSType)))
	}

	if info.Arch == "" {
		info.Arch = "amd64"
		if strings.HasSuffix(deref(instance.Properties.StorageProfile.ImageReference.SKU), "arm64") {
			info.Arch = "arm64"
		}
	}

	if info.Username == "" {
		info.Username = deref(instance.Properties.OSProfile.AdminUsername)
	}
	if info.Username == "" {
		info.Username = "azureuser"
	}

	if info.Password == "" {
		info.Password = deref(instance.Properties.OSProfile.AdminPassword)
	}

	if info.Protocol == "" {
		info.Protocol = provider.ProtocolSSH
		if info.OS == "windows" {
			info.Protocol = provider.ProtocolWinRM
		}
	}

	if err := g.populateNetwork(ctx, &info, instance.VirtualMachineScaleSetVM); err != nil {
		return provider.ConnectInfo{}, err
	}

	return info, nil
}

// Decrease implements provider.InstanceGroup
func (g *InstanceGroup) Decrease(ctx context.Context, instances []string) ([]string, error) {
	if len(instances) == 0 {
		return nil, nil
	}

	instanceIds := make([]*string, 0, len(instances))
	for idx := range instances {
		instanceIds = append(instanceIds, &instances[idx])
	}

	_, err := g.client.BeginDeleteInstances(
		ctx,
		g.ResourceGroupName,
		g.Name,
		armcompute.VirtualMachineScaleSetVMInstanceRequiredIDs{InstanceIDs: instanceIds},
		&armcompute.VirtualMachineScaleSetsClientBeginDeleteInstancesOptions{
			ForceDeletion: to.Ptr(true),
		},
	)
	if err != nil {
		return nil, err
	}

	g.size -= len(instances)

	return instances, nil
}

// Increase implements provider.InstanceGroup
func (g *InstanceGroup) Increase(ctx context.Context, delta int) (int, error) {
	if err := g.setScaleSetSize(ctx, false); err != nil {
		return 0, err
	}

	_, err := g.client.BeginUpdate(
		ctx,
		g.ResourceGroupName,
		g.Name,
		armcompute.VirtualMachineScaleSetUpdate{
			SKU: &armcompute.SKU{
				Capacity: to.Ptr(int64(g.size + delta)),
			},
		},
		nil,
	)
	if err != nil {
		return 0, fmt.Errorf("request to increase vm set capacity: %w", err)
	}

	g.size += delta

	return delta, nil
}

// Update implements provider.InstanceGroup
func (g *InstanceGroup) Update(ctx context.Context, update func(instance string, state provider.State)) error {
	pager := g.client.NewListPager(
		g.ResourceGroupName,
		g.Name,
		&armcompute.VirtualMachineScaleSetVMsClientListOptions{
			Expand: to.Ptr("instanceView"),
		},
	)

	for pager.More() {
		resp, err := pager.NextPage(ctx)
		if err != nil {
			return err
		}

		for _, instance := range resp.Value {
			state := provider.StateCreating

			if instance.Properties.ProvisioningState != nil {
				switch deref(instance.Properties.ProvisioningState) {
				case "Creating", "Updating":
					state = provider.StateCreating

				case "Succeeded":
					state = provider.StateDeleting

					if instance.Properties.InstanceView != nil {
						for _, status := range instance.Properties.InstanceView.Statuses {
							if deref(status.Code) == "PowerState/running" {
								state = provider.StateRunning
								break
							}
						}
					}

				case "Failed", "Deleting":
					state = provider.StateDeleting

				default:
					g.log.Warn("unknown instance state", "instance", instance.InstanceID, "state", deref(instance.Properties.ProvisioningState))
				}
			}

			update(deref(instance.InstanceID), state)
		}
	}

	return nil
}

func (g *InstanceGroup) setScaleSetSize(ctx context.Context, initial bool) error {
	set, err := g.client.GetScaleSet(ctx, g.ResourceGroupName, g.Name, &armcompute.VirtualMachineScaleSetsClientGetOptions{})
	if err != nil {
		return fmt.Errorf("getting scale set size: %w", err)
	}

	capacity := set.SKU.Capacity
	size := int(deref(capacity))

	if !initial && size != g.size {
		g.log.Error("out-of-sync capacity", "expected", g.size, "actual", size)
	}

	g.size = size
	return nil
}

func (g *InstanceGroup) populateNetwork(ctx context.Context, info *provider.ConnectInfo, vm armcompute.VirtualMachineScaleSetVM) error {
	for _, nic := range vm.Properties.NetworkProfileConfiguration.NetworkInterfaceConfigurations {
		if !deref(nic.Properties.Primary) {
			continue
		}

		nicInfo, err := g.client.GetVirtualMachineScaleSetNetworkInterface(
			ctx,
			g.ResourceGroupName,
			g.Name,
			deref(vm.InstanceID),
			deref(nic.Name),
			&armnetwork.InterfacesClientGetVirtualMachineScaleSetNetworkInterfaceOptions{
				Expand: to.Ptr("ipConfigurations/publicIPAddress"),
			},
		)
		if err != nil {
			return fmt.Errorf("getting primary network interface info: %w", err)
		}

		for _, cfg := range nicInfo.Properties.IPConfigurations {
			info.InternalAddr = deref(cfg.Properties.PrivateIPAddress)
			if cfg.Properties.PublicIPAddress != nil && cfg.Properties.PublicIPAddress.Properties != nil {
				info.ExternalAddr = deref(cfg.Properties.PublicIPAddress.Properties.IPAddress)
			}
		}

		return nil
	}

	return fmt.Errorf("found no primary network interface")
}

func (g *InstanceGroup) Shutdown(ctx context.Context) error {
	return nil
}
